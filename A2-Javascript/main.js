 console.log('hello world')
 
 // var, let, const
let scores;
scores = 10;
console.log(scores);

 //let const 
 let score;

 score = 11;

 console.log(score); 

 //const
 const nilai = 12;
  
 console.log(nilai) 

// String, number, Boolean, null, underfind

const name = 'john';
const age = 37;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof age);

// Concatenation
console.log('My name is ' + name + ' and I am ' + age);
// Template String
console.log(`My name is ${name} and I am ${age}`);

const s = 'Hello world';
console.log(s.substring(0, 5));
console.log(s.substring(0, 5).toLocaleUpperCase());
console.log(s.split(''));

const a = 'technology, computers, it, code';
console.log(a.split(',')); 

//Arrays - variable that hold multiple values
const numbers = new Array(1,2,3,4,5);
console.log(numbers);

const fruits = ['apples', 'oranges', 'pears'];
fruits[3] = 'grapes';
fruits.push('mangos');
fruits.unshift('strawberry');
fruits.pop();
console.log(Array.isArray('hello'));
console.log(fruits.indexOf('oranges')); 
console.log(fruits); 

const person = {
    firtsName: 'John',
    lastName: 'Doe',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
        street: '50 main st',
        city: 'Boston',
        state: 'Ma'
    }
}
console.log(person); 
console.log(person.firtsName, person.lastName);
console.log(person.address.city);
console.log(person.hobbies[2]);

const {firstName, lastName, address: {city}} = person
console.log(lastName);

person.email = 'john@gmail.com';
console.log(person);

const todos = [
    {
        id: 1,
        Text: 'take out trash',
        isCompleted: true
    },
    {
        id: 2,
        Text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        Text: 'Dentist',
        isCompleted: false
    },
];
 console.log(todos);
 
 //For 
 for(let i =0; i <=10; i++) {
    console.log('For Loop Number : ${i}');
 }

 // While
 let i = 0;
 while(i < 10) {
    console.log('While Loop Number : ${i}'); 
    i++;
 }

 // HIGH ORDER ARRAY METHODS (show prototype)

// forEach() - Loops through array
todos.forEach(function(todo, i, myTodos) {
    console.log(`${i + 1}: ${todo.text}`);
    console.log(myTodos);
  });

  // map() - Loop through and create new array
const todoTextArray = todos.map(function(todo) {
    return todo.text;
  });
  console.log(todoTextArray);

// filter() - Returns array based on condition
const todo1 = todos.filter(function(todo) {
  // Return only todos where id is 1
  return todo.id === 1; 
});

//switch 
color = 'blue';

switch(color) {
  case 'red':
    console.log('color is red');
  case 'blue':
    console.log('color is blue');
  default:  
    console.log('color is not red or blue')
}

function addNums(num1 = 1, num2 = 1) {
    console.log(num1 + num2);
}
addNums(5,5);

//Constructor function
function Person(firstName, lastname, dob){
    this.firstName = firstName;
    this.lastName =lastName;
    this.dob = new Date(dob);
    this.getBirthYear = function() {
        return this.dob.getFullYear();
    }
    this.getFullName - function() {
        return '${this.firstName} ${this.lastName}';
    }


    person.protoype = function() {
        return this.dob.getFullYear();
    }
    person.protoype.getFullName - function() {
        return '${this.firstName} ${this.lastName}';
    }
}


//Instantiate object
const person1 = new Person('John', 'Doe', '4-3-1080');
const person2 = new Person('Mary', 'Smith', '3-6-1970');
console.log(person1);



// ELEMENT SELECTORS

// Single Element Selectors
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
// // Multiple Element Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));

// MANIPULATING THE DOM
const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

const btn = document.querySelector('.btn');
btn.style.background = 'red';

// EVENTS

// Mouse Event
btn.addEventListener('click', e => {
    e.preventDefault();
    console.log(e.target.className);
    document.getElementById('my-form').style.background = '#ccc';
    document.querySelector('body').classList.add('bg-dark');
    ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
  });

  // USER FORM SCRIPT

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

function onSubmit(e) {
    e.preventDefault();
    
    if(nameInput.value === '' || emailInput.value === '') {
      // alert('Please enter all fields');
      msg.classList.add('error');
      msg.innerHTML = 'Please enter all fields';
  
      // Remove error after 3 seconds
      setTimeout(() => msg.remove(), 3000);
    } else {
      // Create new list item with user
      const li = document.createElement('li');
  
      // Add text node with input values
      li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));
  
      // Add HTML
      // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;
  
      // Append to ul
      userList.appendChild(li);
  
      // Clear fields
      nameInput.value = '';
      emailInput.value = '';
    }

}
  
